import axios from "axios";

const baseUrl = "http://138.197.97.162";

let api = axios.create({
    baseURL: baseUrl,
});
api.defaults.timeout = 10000;

export default api;
