import { all } from "redux-saga/effects";
import Pedido from "./pizza/saga";

export default function* rootSaga() {
    return yield all([Pedido]);
}
