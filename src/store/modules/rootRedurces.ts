import { combineReducers } from "redux";
import pizza from "./pizza/reducer";

export default combineReducers({
    pizza,
});
