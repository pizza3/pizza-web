import produce from "immer";

interface Initial_StateTypes {
    tamanho: Object | null;
    massa: Object | null;
    borda: Object | null;
    recheio: Object | null;
}

const INITIAL_STATE: Initial_StateTypes = {
    tamanho: null,
    massa: null,
    borda: null,
    recheio: null,
};

export default function dados(state = INITIAL_STATE, action: any) {
    return produce(state, (draft) => {
        switch (action.type) {
            case "@pizza/TAMANHO": {
                draft.tamanho = action.payload.props;
                break;
            }
            case "@pizza/MASSA": {
                draft.massa = action.payload.props;
                break;
            }
            case "@pizza/BORDA": {
                draft.borda = action.payload.props;
                break;
            }
            case "@pizza/RECHEIO": {
                draft.recheio = action.payload.props;
                break;
            }
            case "@pizza/LIMPAR": {
                draft.tamanho = null;
                draft.massa = null;
                draft.borda = null;
                draft.recheio = null;
                break;
            }
            default:
        }
    });
}
