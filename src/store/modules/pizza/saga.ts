import { takeLatest, all, select, call } from "redux-saga/effects";
import api from "../../../service";
import { toast } from "react-toastify";

export function* pedido() {
    const state = yield select((state: any) => state.pizza);

    try {
        const response = yield call(api.post, "pedido", {
            tamanho_id: state.tamanho.id,
            massa_id: state.massa.id,
            borda_id: state.borda.id,
            recheio_id: state.recheio.id,
        });

        const { message } = response.data;
        const notify = () => toast.success(message);
        notify();
    } catch (err: any) {
        const notify = () => toast.error(err.response.data.message);
        notify();
    }
}

export default all([takeLatest("#pizza/BENEFICIOS", pedido)]);
