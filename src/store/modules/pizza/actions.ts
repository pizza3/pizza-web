export function tamanhoPizza(props: Object) {
    return {
        type: "@pizza/TAMANHO",
        payload: {
            props,
        },
    };
}
export function massaPizza(props: Object) {
    return {
        type: "@pizza/MASSA",
        payload: {
            props,
        },
    };
}

export function bordaPizza(props: Object) {
    return {
        type: "@pizza/BORDA",
        payload: {
            props,
        },
    };
}
export function recheioPizza(props: Object) {
    return {
        type: "@pizza/RECHEIO",
        payload: {
            props,
        },
    };
}

export function limparPizza() {
    return {
        type: "@pizza/LIMPAR",
    };
}
export function requestPizza() {
    return {
        type: "#pizza/BENEFICIOS",
    };
}
