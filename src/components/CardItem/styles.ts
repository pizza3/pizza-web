import { makeStyles, createStyles, Theme } from "@material-ui/core/styles";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1,
        },
        paper: {
            padding: theme.spacing(2),
            color: theme.palette.text.secondary,
        },
        containerCard: {
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between",
        },
        containerDescription: {
            marignTop: 7,
            marginBottom: 7,
            minHeight: 60,
        },
    })
);

export default useStyles;
