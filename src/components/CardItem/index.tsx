import React, { useEffect, useState } from "react";
import Paper from "@material-ui/core/Paper";
import { Grid, Box } from "@material-ui/core";
import { CardItemTypes } from "./types";
import useStyles from "./styles";
import { Typography } from "@material-ui/core";

const CardItem: React.FC<CardItemTypes> = ({
    id,
    name,
    description,
    score,
    price,
    promotion_day_start,
    promotion_day_end,
    onClick,
}) => {
    const classes = useStyles();

    const [itemPromocional, setItemPromocional] = useState<boolean>(false);
    let dayWeek = new Date().getDay();

    useEffect(() => {
        if (dayWeek >= promotion_day_start && dayWeek <= promotion_day_end) {
            setItemPromocional(true);
        }
    });

    return (
        <Grid
            item
            xs={12}
            md={4}
            spacing={2}
            key={id}
            onClick={() => {
                onClick();
            }}
        >
            <Paper className={classes.paper}>
                <Typography>
                    <Box fontWeight="fontWeightBold">{name}</Box>
                </Typography>

                {description ? (
                    <div className={classes.containerDescription}>
                        <Typography variant="subtitle2">
                            {description}
                        </Typography>
                    </div>
                ) : null}
                <div className={classes.containerCard}>
                    <Typography>{`R$ ${price}`}</Typography>
                    {itemPromocional ? (
                        <Typography>Benefício: {score}pt</Typography>
                    ) : null}
                </div>
            </Paper>
        </Grid>
    );
};

export default CardItem;
