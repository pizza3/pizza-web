import React from "react";
import store from "./store";
import { Provider } from "react-redux";
import Routes from "./routes/app.routes";

import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

function App() {
    return (
        <Provider store={store}>
            <Routes />
            <ToastContainer />
        </Provider>
    );
}

export default App;
