import React from "react";

import Home from "../pages/home";
import Tamanho from "../pages/tamanho";
import Massa from "../pages/massa";
import Borda from "../pages/borda";
import Recheio from "../pages/recheio";
import Pedido from "../pages/pedido";

import {
    BrowserRouter as Router,
    Route,
    Switch,
    Redirect,
} from "react-router-dom";
import { ToastContainer } from "react-toastify";

import "react-toastify/dist/ReactToastify.css";

import { Flip } from "react-toastify";

const Themes: React.FC = () => {
    return (
        <>
            <ToastContainer transition={Flip} />
            <Router>
                <Switch>
                    <Route path="/" exact component={Home} />
                    <Route path="/tamanho" exact component={Tamanho} />
                    <Route path="/massa" exact component={Massa} />
                    <Route path="/borda" exact component={Borda} />
                    <Route path="/recheio" exact component={Recheio} />
                    <Route path="/pedido" exact component={Pedido} />
                    <Route path="*" component={() => <Redirect to="/" />} />
                </Switch>
            </Router>
        </>
    );
};

export default Themes;
