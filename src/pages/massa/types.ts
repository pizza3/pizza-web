export interface TamanhosStateTypes {
    id: number;
    name: string;
    description?: string;
    score: number;
    price: number;
    promotion_day_start: number;
    promotion_day_end: number;
}
