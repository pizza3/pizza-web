import React, { useEffect, useState } from "react";

import { Typography, Grid } from "@material-ui/core";

import { useStyles } from "./styles";
import CardItem from "../../components/CardItem";
import api from "../../service";
import { TamanhosStateTypes } from "./types";

import { useDispatch } from "react-redux";
import { massaPizza } from "../../store/modules/pizza/actions";
import { useHistory } from "react-router-dom";
import { useSelector } from "react-redux";

const Massas: React.FC = () => {
    const [massas, setMassas] = useState<[]>([]);
    const [dailyDeal, setDailyDeal] = useState<[]>([]);
    const state = useSelector((state: any) => state.pizza);

    let history = useHistory();
    const dispatch = useDispatch();
    useEffect(() => {
        if (!state.tamanho) {
            history.push("/");
        }
        async function requestTamanhos() {
            try {
                const request = await api.get("massas");
                setMassas(request.data.all);
                setDailyDeal(request.data.dailyDeal);
            } catch (err) {}
        }
        requestTamanhos();
    }, []);

    const classes = useStyles();
    return (
        <div className={classes.root}>
            <Grid item xs={11} md={11} className={classes.gridContainer}>
                <Typography variant="h4" className={classes.title}>
                    Massa
                </Typography>
                <Typography variant="h6" className={classes.title}>
                    Promoção do dia - ganhe benefícios
                </Typography>
                <Grid container spacing={2}>
                    {dailyDeal.map((props: TamanhosStateTypes) => (
                        <CardItem
                            id={props.id}
                            name={props.name}
                            description={props.description}
                            price={props.price}
                            score={props.score}
                            promotion_day_start={props.promotion_day_start}
                            promotion_day_end={props.promotion_day_end}
                            onClick={() => {
                                history.push("borda");
                                dispatch(massaPizza(props));
                            }}
                        />
                    ))}
                </Grid>
                <Typography variant="h6" className={classes.title}>
                    Todas as massas
                </Typography>
                <Grid container spacing={2}>
                    {massas.map((props: TamanhosStateTypes) => (
                        <CardItem
                            id={props.id}
                            name={props.name}
                            description={props.description}
                            price={props.price}
                            score={props.score}
                            promotion_day_start={props.promotion_day_start}
                            promotion_day_end={props.promotion_day_end}
                            onClick={() => {
                                history.push("borda");
                                dispatch(massaPizza(props));
                            }}
                        />
                    ))}
                </Grid>
            </Grid>
        </div>
    );
};

export default Massas;
