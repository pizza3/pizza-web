import React, { useEffect, useState } from "react";

import { Typography, Grid } from "@material-ui/core";

import { useStyles } from "./styles";
import CardItem from "../../components/CardItem";
import api from "../../service";
import { TamanhosStateTypes } from "./types";
import { useSelector } from "react-redux";

import { useDispatch } from "react-redux";
import { bordaPizza } from "../../store/modules/pizza/actions";
import { useHistory } from "react-router-dom";

const Borda: React.FC = () => {
    const [bordas, setBordas] = useState<[]>([]);
    const [dailyDeal, setDailyDeal] = useState<[]>([]);
    const state = useSelector((state: any) => state.pizza);

    let history = useHistory();
    const dispatch = useDispatch();
    useEffect(() => {
        if (!state.tamanho || !state.massa) {
            history.push("/");
        }
        async function requestTamanhos() {
            try {
                const request = await api.get("bordas");
                setBordas(request.data.all);
                setDailyDeal(request.data.dailyDeal);
            } catch (err) {}
        }
        requestTamanhos();
    }, []);

    const classes = useStyles();
    return (
        <div className={classes.root}>
            <Grid item xs={11} md={11} className={classes.gridContainer}>
                <Typography variant="h4" className={classes.title}>
                    Borda
                </Typography>
                <Typography variant="h5" className={classes.title}>
                    Promoção do dia - ganhe benefícios
                </Typography>

                <Grid container spacing={2}>
                    {dailyDeal.map((props: TamanhosStateTypes) => (
                        <CardItem
                            promotion_day_start={props.promotion_day_start}
                            promotion_day_end={props.promotion_day_end}
                            score={props.score}
                            id={props.id}
                            name={props.name}
                            price={props.price}
                            onClick={() => {
                                history.push("recheio");
                                dispatch(bordaPizza(props));
                            }}
                        />
                    ))}
                </Grid>
                <Typography variant="h6" className={classes.title}>
                    Todas as bordas
                </Typography>
                <Grid container spacing={2}>
                    {bordas.map((props: TamanhosStateTypes) => (
                        <CardItem
                            promotion_day_start={props.promotion_day_start}
                            promotion_day_end={props.promotion_day_end}
                            score={props.score}
                            id={props.id}
                            name={props.name}
                            price={props.price}
                            onClick={() => {
                                history.push("recheio");
                                dispatch(bordaPizza(props));
                            }}
                        />
                    ))}
                </Grid>
            </Grid>
        </div>
    );
};

export default Borda;
