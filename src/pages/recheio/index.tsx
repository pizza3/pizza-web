import React, { useEffect, useState } from "react";

import { Typography, Grid } from "@material-ui/core";

import { useStyles } from "./styles";
import CardItem from "../../components/CardItem";
import api from "../../service";
import { TamanhosStateTypes } from "./types";
import { useHistory } from "react-router-dom";
import { useDispatch } from "react-redux";
import { recheioPizza } from "../../store/modules/pizza/actions";
import { useSelector } from "react-redux";

const Recheio: React.FC = () => {
    const [recheios, setRecheios] = useState<[]>([]);
    const [dailyDeal, setDailyDeal] = useState<[]>([]);
    const state = useSelector((state: any) => state.pizza);

    let history = useHistory();
    const dispatch = useDispatch();
    useEffect(() => {
        if (!state.tamanho || !state.massa || !state.borda) {
            history.push("/");
        }

        async function requestTamanhos() {
            try {
                const request = await api.get("recheios");
                setRecheios(request.data.all);
                setDailyDeal(request.data.dailyDeal);
            } catch (err) {}
        }
        requestTamanhos();
    }, []);

    const classes = useStyles();
    return (
        <div className={classes.root}>
            <Grid item xs={11} md={11} className={classes.gridContainer}>
                <Typography variant="h4" className={classes.title}>
                    Recheio
                </Typography>
                <Typography variant="h6" className={classes.title}>
                    Promoção do dia - ganhe benefícios
                </Typography>
                <Grid container spacing={2}>
                    {dailyDeal.map((props: TamanhosStateTypes) => (
                        <CardItem
                            id={props.id}
                            name={props.name}
                            description={props.description}
                            price={props.price}
                            score={props.score}
                            promotion_day_start={props.promotion_day_start}
                            promotion_day_end={props.promotion_day_end}
                            onClick={() => {
                                history.push("pedido");
                                dispatch(recheioPizza(props));
                            }}
                        />
                    ))}
                </Grid>
                <Typography variant="h6" className={classes.title}>
                    Todos os recheios
                </Typography>
                <Grid container spacing={2}>
                    {recheios.map((props: TamanhosStateTypes) => (
                        <CardItem
                            id={props.id}
                            name={props.name}
                            description={props.description}
                            price={props.price}
                            score={props.score}
                            promotion_day_start={props.promotion_day_start}
                            promotion_day_end={props.promotion_day_end}
                            onClick={() => {
                                history.push("pedido");
                                dispatch(recheioPizza(props));
                            }}
                        />
                    ))}
                </Grid>
            </Grid>
        </div>
    );
};

export default Recheio;
