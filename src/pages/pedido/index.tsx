import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
    Grid,
    Button,
    Typography,
    Card,
    CardActionArea,
    CardActions,
    CardContent,
    CardMedia,
} from "@material-ui/core";
import Imagem from "../../assets/img/pizza2.jpg";

import { useStyles } from "./styles";
import Divider from "@material-ui/core/Divider";
import { useHistory } from "react-router-dom";

import { limparPizza, requestPizza } from "../../store/modules/pizza/actions";

const Pedido: React.FC = () => {
    const [stateTrue, setStateTrue] = useState<boolean>(false);

    const state = useSelector((state: any) => state.pizza);
    const router = useHistory();
    const dispatch = useDispatch();

    useEffect(() => {
        if (!state.tamanho || !state.massa || !state.borda || !state.recheio) {
            router.push("/");
        } else {
            setStateTrue(true);
        }
    }, [state.tamanho]);

    function calcScore() {
        let dayWeek = new Date().getDay();
        let score: number = 0;

        if (
            state.tamanho.promotion_day_start <= dayWeek &&
            state.tamanho.promotion_day_end >= dayWeek
        ) {
            score += state.tamanho.score;
        }
        if (
            state.massa.promotion_day_start <= dayWeek &&
            state.massa.promotion_day_end >= dayWeek
        ) {
            score += state.massa.score;
        }
        if (
            state.borda.promotion_day_start <= dayWeek &&
            state.borda.promotion_day_end >= dayWeek
        ) {
            score += state.borda.score;
        }
        if (
            state.recheio.promotion_day_start <= dayWeek &&
            state.recheio.promotion_day_end >= dayWeek
        ) {
            score += state.recheio.score;
        }
        return score;
    }

    const classes = useStyles();
    return (
        <div className={classes.root}>
            <Grid container xs={12} md={12} className={classes.gridContainer}>
                <Grid container xs={11} md={5}>
                    <Card className={classes.rootCard}>
                        <CardActionArea>
                            <CardMedia
                                className={classes.media}
                                image={Imagem}
                            />
                        </CardActionArea>
                        <CardContent>
                            <Typography
                                gutterBottom
                                variant="h5"
                                component="h2"
                            >
                                Detalhe
                            </Typography>
                            {stateTrue ? (
                                <>
                                    <div className={classes.containerText}>
                                        <Typography>
                                            Pontos de beneficius
                                        </Typography>
                                        <Typography>
                                            {`${calcScore()}pt
                                            `}
                                        </Typography>
                                    </div>
                                    <div className={classes.containerText}>
                                        <Typography>Tamanho</Typography>
                                        <Typography>{`${
                                            state.tamanho.name
                                        } - R$ ${parseFloat(
                                            state.tamanho.price
                                        ).toFixed(2)}`}</Typography>
                                    </div>
                                    <div className={classes.containerText}>
                                        <Typography>Massa</Typography>
                                        <Typography>{`${
                                            state.massa.name
                                        } - R$ ${parseFloat(
                                            state.massa.price
                                        ).toFixed(2)}`}</Typography>
                                    </div>
                                    <div className={classes.containerText}>
                                        <Typography>Borda</Typography>
                                        <Typography>{`${
                                            state.borda.name
                                        } - R$ ${parseFloat(
                                            state.borda.price
                                        ).toFixed(2)}`}</Typography>
                                    </div>
                                    <div className={classes.containerText}>
                                        <Typography>Recheio</Typography>
                                        <Typography>{`${
                                            state.recheio.name
                                        } - R$ ${parseFloat(
                                            state.recheio.price
                                        ).toFixed(2)}`}</Typography>
                                    </div>
                                    <div className={classes.containerText}>
                                        <Typography>Valor total</Typography>
                                        <Typography>
                                            {`R$ ${
                                                state.tamanho.price +
                                                state.massa.price +
                                                state.borda.price +
                                                state.recheio.price
                                            }`}
                                        </Typography>
                                    </div>
                                </>
                            ) : null}
                            <Divider />
                        </CardContent>
                        <CardActions>
                            <div className={classes.containerButton}>
                                <Button
                                    size="small"
                                    color="primary"
                                    onClick={() => {
                                        router.push("/");
                                        dispatch(limparPizza());
                                    }}
                                >
                                    Cancelar
                                </Button>
                                <Button
                                    size="small"
                                    color="primary"
                                    onClick={() => {
                                        dispatch(requestPizza());
                                        router.push("/");
                                        dispatch(limparPizza());
                                    }}
                                >
                                    Finalizar
                                </Button>
                            </div>
                        </CardActions>
                    </Card>
                </Grid>
            </Grid>
        </div>
    );
};

export default Pedido;
