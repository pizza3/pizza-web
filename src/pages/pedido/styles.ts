import { makeStyles, createStyles, Theme } from "@material-ui/core/styles";

export const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1,
        },
        rootCard: {
            flexGrow: 1,
            marginTop: 20,
        },
        paper: {
            padding: theme.spacing(2),
            textAlign: "center",
            color: theme.palette.text.secondary,
        },
        title: {
            margin: 20,
        },
        gridContainer: {
            display: "flex",
            justifyContent: "center",
        },

        containerText: {
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between",
        },
        containerButton: {
            display: "flex",
            flexDirection: "row",
            justifyContent: "flex-end",
            width: "100%",
        },
        media: {
            height: 140,
        },
    })
);
