import React, { useEffect, useState } from "react";

import { Typography, Grid } from "@material-ui/core";

import { useStyles } from "./styles";
import CardItem from "../../components/CardItem";
import api from "../../service";
import { TamanhosStateTypes } from "./types";
import { useDispatch } from "react-redux";
import { tamanhoPizza } from "../../store/modules/pizza/actions";
import { useHistory } from "react-router-dom";

const Tamanho: React.FC = () => {
    const [tamanhos, setTamanhos] = useState<[]>([]);
    const [dailyDeal, setDailyDeal] = useState<[]>([]);
    let history = useHistory();
    const dispatch = useDispatch();
    useEffect(() => {
        async function requestTamanhos() {
            try {
                const request = await api.get("tamanhos");
                setTamanhos(request.data.all);
                setDailyDeal(request.data.dailyDeal);
            } catch (err) {}
        }
        requestTamanhos();
    }, []);

    const classes = useStyles();
    return (
        <div className={classes.root}>
            <Grid item xs={11} md={11} className={classes.gridContainer}>
                <Typography variant="h4" className={classes.title}>
                    Tamanhos
                </Typography>
                <Typography variant="h6" className={classes.title}>
                    Promoção do dia - ganhe benefícios
                </Typography>
                <Grid container spacing={2}>
                    {dailyDeal.map((props: TamanhosStateTypes) => (
                        <CardItem
                            promotion_day_start={props.promotion_day_start}
                            promotion_day_end={props.promotion_day_end}
                            score={props.score}
                            id={props.id}
                            name={props.name}
                            price={props.price}
                            onClick={() => {
                                history.push("massa");
                                dispatch(tamanhoPizza(props));
                            }}
                        />
                    ))}
                </Grid>
                <Typography variant="h6" className={classes.title}>
                    Todos os tamanhos
                </Typography>
                <Grid container spacing={2}>
                    {tamanhos.map((props: TamanhosStateTypes) => (
                        <CardItem
                            promotion_day_start={props.promotion_day_start}
                            promotion_day_end={props.promotion_day_end}
                            score={props.score}
                            id={props.id}
                            name={props.name}
                            price={props.price}
                            onClick={() => {
                                history.push("massa");
                                dispatch(tamanhoPizza(props));
                            }}
                        />
                    ))}
                </Grid>
            </Grid>
        </div>
    );
};

export default Tamanho;
