export interface TamanhosStateTypes {
    id: number;
    name: string;
    score: number;
    price: number;
    promotion_day_start: number;
    promotion_day_end: number;
}
