import { makeStyles, createStyles, Theme } from "@material-ui/core/styles";

export const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1,
        },
        paper: {
            padding: theme.spacing(2),
            textAlign: "center",
            color: theme.palette.text.secondary,
        },
        title: {
            margin: 20,
        },
        gridContainer: {
            marginLeft: 10,
        },
        marginTop: {
            marginTop: 40,
        },
    })
);
