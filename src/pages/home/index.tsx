import React from "react";
import CssBaseline from "@material-ui/core/CssBaseline";
import Grid from "@material-ui/core/Grid";
import ButtonBase from "@material-ui/core/ButtonBase";
import Typography from "@material-ui/core/Typography";
import { useHistory } from "react-router-dom";

import { useStyles } from "./styles";

export default function SignInSide() {
    const classes = useStyles();
    const history = useHistory();
    return (
        <Grid container component="main" className={classes.container}>
            <CssBaseline />
            <Grid item xs={12} sm={12} md={12} className={classes.imageGrid}>
                <div className={classes.containerButton}>
                    <ButtonBase
                        focusRipple
                        key="Breakfast"
                        className={classes.image}
                        focusVisibleClassName={classes.focusVisible}
                        style={{
                            width: "40%",
                        }}
                    >
                        <span
                            className={classes.imageButton}
                            onClick={() => history.push("tamanho")}
                        >
                            <Typography
                                component="span"
                                variant="h4"
                                color="inherit"
                                className={classes.imageTitle}
                            >
                                Faça seu pedido
                                <span className={classes.imageMarked} />
                            </Typography>
                        </span>
                    </ButtonBase>
                </div>
            </Grid>
        </Grid>
    );
}
