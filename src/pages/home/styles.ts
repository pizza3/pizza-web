import { makeStyles, createStyles, Theme } from "@material-ui/core/styles";

import Image from "../../assets/img/pizza.jpg";

export const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        container: {
            //
            height: "100vh",
        },
        imageGrid: {
            //
            backgroundImage: `url(${Image})`,
            backgroundRepeat: "no-repeat",
            backgroundSize: "cover",
            backgroundPosition: "center",
        },
        containerButton: {
            //
            display: "flex",
            flexDirection: "column-reverse",
            height: "90vh",
            alignItems: "center",
        },
        image: {
            //
            position: "relative",
            height: 200,
            [theme.breakpoints.down("xs")]: {
                width: "100% !important",
                height: 100,
            },
            "&:hover, &$focusVisible": {
                zIndex: 1,
                "& $imageBackdrop": {
                    opacity: 0.15,
                },
                "& $imageMarked": {
                    opacity: 0,
                },
                "& $imageTitle": {
                    border: "4px solid currentColor",
                },
            },
        },
        focusVisible: {}, //
        imageButton: {
            //
            position: "absolute",
            left: 0,
            right: 0,
            top: 0,
            bottom: 0,
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            color: "#00ff00", //   theme.palette.common.white,
        },

        imageTitle: {
            //
            position: "relative",
            padding: `${theme.spacing(2)}px ${theme.spacing(4)}px ${
                theme.spacing(1) + 6
            }px`,
        },
        imageMarked: {
            //
            height: 3,
            width: 18,
            color: "#00ff00",
            position: "absolute",
            bottom: -2,
        },
    })
);
